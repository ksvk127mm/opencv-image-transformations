# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* A C++ opencv app that implements several image transformations

### How do I get set up? ###

* The project was made at the Jetbrains Clion IDE, however the code can be compiled
by running the makefile provided at the cmake-build-debug folder
* Sample images are provided at the cmake-build-debug/Images folder. To use another image set the IMAGE_PATH #define at main.cpp to the appropriate path

### Who do I talk to? ###

* Repo owner or admin