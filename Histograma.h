#ifndef RGB_CMYK_HSI_HISTOGRAMA_H
#define RGB_CMYK_HSI_HISTOGRAMA_H

#include <vector>

#include "Imagem.h"
#include "libs/pbPlots.hpp"
#include "libs/supportLib.hpp"

using namespace std;

class Histograma
{
    private:
        vector<vector<double>> histograms;
        vector<double> gray_histogram;

        double average_intensity(int, int, double);
        double sum_probability(int, int);
        double calculate_variance(int);
    public:
        Histograma(vector<vector<double>>);
        Histograma(vector<double>);
        void plotar(int, bool grayscale = false);
        static void sum(vector<double>);
        void normalizar_rgb(double);
        void normalizar_gray(double);
        void accumular();
        Imagem equalizar_imagem(Imagem &);
        int otsu_method();

        const vector<vector<double>> &getHistograms() const;
        void setHistograms(const vector<vector<double>> &);
        const vector<double> &getGrayHistogram() const;
        void setGrayHistogram(const vector<double> &grayHistogram);
};

#endif //RGB_CMYK_HSI_HISTOGRAMA_H
