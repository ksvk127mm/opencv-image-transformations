#include "Histograma.h"

#include <cmath>
#include <utility>

Histograma::Histograma(vector<vector<double>> histogramas)
{
    this->histograms = move(histogramas);
}

Histograma::Histograma(vector<double> gray_histogram)
{
    this->gray_histogram = gray_histogram;
}

void Histograma::sum(vector<double> array)
{
    double soma = 0.0;

    for(double &i : array)
        soma += i;

    cout << soma;
}

void Histograma::normalizar_rgb(double total_pixels)
{
    for(int i = 0; i < histograms[0].size(); i++){
        histograms[0][i] /= total_pixels;
        histograms[1][i] /= total_pixels;
        histograms[2][i] /= total_pixels;
    }
}

void Histograma::normalizar_gray(double total_pixels)
{
    for(double & i : gray_histogram){
        i /= total_pixels;
    }
}

void Histograma::accumular()
{
    vector<vector<double>> hist_acc(3, vector<double>(256, 0.0));

    for(int i = 0; i < hist_acc[0].size(); i++){
        hist_acc[0][i] = (i != 0 ? hist_acc[0][i - 1] : 0) + histograms[0][i];
        hist_acc[1][i] = (i != 0 ? hist_acc[1][i - 1] : 0) + histograms[1][i];
        hist_acc[2][i] = (i != 0 ? hist_acc[2][i - 1] : 0) + histograms[2][i];
    }

    histograms = hist_acc;
}

Imagem Histograma::equalizar_imagem(Imagem &img)
{
    Mat imgEq;
    imgEq.create(img.get_rows(), img.get_cols(), CV_8UC3);

    for(int i = 0; i < img.get_rows(); i++){
        for(int j = 0; j < img.get_cols(); j++){
            Vec3b pixel = img.getImage().at<Vec3b>(i, j);
            pixel[0] = (uint8_t) (255 * histograms[0][pixel[0]]);
            pixel[1] = (uint8_t) (255 * histograms[1][pixel[1]]);
            pixel[2] = (uint8_t) (255 * histograms[2][pixel[2]]);
            imgEq.at<Vec3b>(i, j) = pixel;
        }
    }

    Imagem resultado(imgEq);

    return resultado;
}

double Histograma::average_intensity(int begin, int end, double probability)
{
    double intesity = 0;

    for(int i = begin; i < end; i++){
        intesity += gray_histogram[i] * i;
    }

    return intesity / probability;
}

double Histograma::sum_probability(int begin, int end)
{
    double probability = 0;

    for(int i = begin; i < end; i++){
        probability += gray_histogram[i];
    }

    return probability;
}

double Histograma::calculate_variance(int threshold)
{
    double c1_prob = sum_probability(0, threshold + 1);
    double c2_prob = 1 - c1_prob;

    double m1 = average_intensity(0, threshold + 1, c1_prob);
    double m2 = average_intensity(threshold + 1, (int) gray_histogram.size(), c2_prob);
    double mg = average_intensity(0, (int) gray_histogram.size(), 1.0);

    double variance = c1_prob * pow(m1 - mg, 2) + c2_prob * pow(m2 - mg, 2);

    return variance;
}

int Histograma::otsu_method()
{
    int optimal_threshold = 0;
    double max_variance = 0;

    for(int threshold = 0; threshold < gray_histogram.size(); threshold++){
        double variance = calculate_variance(threshold);
        if(variance > max_variance){
            max_variance = variance;
            optimal_threshold = threshold;
        }
    }

    return optimal_threshold;
}

void Histograma::plotar(int channel, bool grayscale)
{
    RGBABitmapImageReference *imageRef = CreateRGBABitmapImageReference();

    vector<double> x(256);
    for(int i = 0; i <= x.size(); i++)
        x[i] = i;

    vector<double> y;
    if(grayscale)
        y = gray_histogram;
    else
        y = histograms[channel];

    DrawScatterPlot(imageRef, 1920, 1080, &x, &y);

    vector<double> *pngData = ConvertToPNG(imageRef->image);
    WriteToFile(pngData, "plot.png");
    DeleteImage(imageRef->image);

    Imagem img("plot.png");
    img.rederizar();
}

const vector<vector<double>> &Histograma::getHistograms() const {
    return histograms;
}

void Histograma::setHistograms(const vector<vector<double>> &histograms) {
    Histograma::histograms = histograms;
}

const vector<double> &Histograma::getGrayHistogram() const {
    return gray_histogram;
}

void Histograma::setGrayHistogram(const vector<double> &grayHistogram) {
    gray_histogram = grayHistogram;
}
