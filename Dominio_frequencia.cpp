#include "Dominio_frequencia.h"
#include <cmath>
#include <iostream>

Dominio_frequencia::Dominio_frequencia(Mat src)
{
    src.copyTo(dom_freq);
}

Mat Dominio_frequencia::inverse_discrete_cosine_transform()
{
    int channels = dom_freq.channels();
    int m = dom_freq.rows, n = dom_freq.cols;

    Mat i_dct;
    i_dct.create(m, n, CV_8UC3);

    double cu, cv, i_dct1, sum;

    for(int channel = 0; channel < channels; channel++){
        for (int x = 0; x < m; x++) {
            for (int y = 0; y < n; y++) {

                sum = 0;
                for (int u = 0; u < m; u++) {
                    for (int v = 0; v < n; v++) {

                        cu = u == 0 ? 1 / sqrt(m) : sqrt(2) / sqrt(m);
                        cv = v == 0 ? 1 / sqrt(n) : sqrt(2) / sqrt(n);

                        i_dct1 = dom_freq.at<Vec3d>(u, v)[channel] *
                                cos(((2 * x + 1) * u * M_PI) / (2 * m)) *
                                cos(((2 * y + 1) * v * M_PI) / (2 * n));

                        sum += cu * cv * i_dct1;
                    }
                }
                i_dct.at<Vec3b>(x, y)[channel] = (uint8_t) round(sum);
            }
        }
    }

    return i_dct;
}

double eucledian_distance(int x_1, int y_1, int x_2, int y_2)
{
    return sqrt(pow(x_2 - x_1, 2) + pow(y_2 - y_1, 2) * 1.0);
}

void Dominio_frequencia::low_pass_filter(int x, int y, double distancia)
{
    int m = dom_freq.rows;
    int n = dom_freq.cols;

    double fator;

    for(int u = 0; u < m; u++){
        for(int v = 0; v < n; v++){
            if(eucledian_distance(x, y, u, v) <= distancia)
                fator = 1.0;
            else
                fator = 0.0;
            dom_freq.at<Vec3d>(u, v)[0] *= fator;
            dom_freq.at<Vec3d>(u, v)[1] *= fator;
            dom_freq.at<Vec3d>(u, v)[2] *= fator;
        }
    }
}
void Dominio_frequencia::high_pass_filter(int x, int y, double distancia)
{
    int m = dom_freq.rows;
    int n = dom_freq.cols;

    double fator;

    for(int u = 0; u < m; u++){
        for(int v = 0; v < n; v++){
            if(eucledian_distance(x, y, u, v) <= distancia)
                fator = 0.0;
            else
                fator = 1.0;
            dom_freq.at<Vec3d>(u, v)[0] *= fator;
            dom_freq.at<Vec3d>(u, v)[1] *= fator;
            dom_freq.at<Vec3d>(u, v)[2] *= fator;
        }
    }
}


void Dominio_frequencia::insert_noise(int x, int y, double size, double value)
{
    int m = dom_freq.rows;
    int n = dom_freq.cols;

    for(int u = 0; u < m; u++){
        for(int v = 0; v < n; v++){
            if(eucledian_distance(x, y, u, v) <= size){
                dom_freq.at<Vec3d>(u, v)[0] = value;
                dom_freq.at<Vec3d>(u, v)[1] = value;
                dom_freq.at<Vec3d>(u, v)[2] = value;
            }
        }
    }
}

const Mat &Dominio_frequencia::getDomFreq() const {
    return dom_freq;
}

void Dominio_frequencia::setDomFreq(const Mat &domFreq) {
    dom_freq = domFreq;
}