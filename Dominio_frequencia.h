#ifndef RGB_CMYK_HSI_DOMINIO_FREQUENCIA_H
#define RGB_CMYK_HSI_DOMINIO_FREQUENCIA_H

#include "Imagem.h"
#include "opencv2/core.hpp"

using namespace cv;

class Dominio_frequencia
{
    private:
        Mat dom_freq;
    public:
        Dominio_frequencia(Mat);
        const Mat &getDomFreq() const;
        void setDomFreq(const Mat &domFreq);
        void low_pass_filter(int, int, double);
        void high_pass_filter(int, int, double);
        void insert_noise(int x, int y, double size, double value);
        Mat inverse_discrete_cosine_transform();
};

#endif //RGB_CMYK_HSI_DOMINIO_FREQUENCIA_H
