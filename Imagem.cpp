#include "Imagem.h"

#include <iostream>
#include <cmath>

Imagem::Imagem(const string& image_path)
{
    image = imread(image_path);
    if(this->image.empty()){
        throw open_img_exception();
    }
}

Imagem::Imagem(const Mat& src)
{
    src.copyTo(image);
}

void Imagem::rederizar()
{
    namedWindow("window", WINDOW_NORMAL);
    setWindowProperty("window", WND_PROP_FULLSCREEN, WINDOW_FULLSCREEN);
    imshow("window", image);

    waitKey(0);

    destroyAllWindows();
}

void Imagem::rederizar(int channel)
{
    vector<Mat> splitChannels(image.channels());
    split(image, splitChannels);

    namedWindow("window", WINDOW_NORMAL);
    setWindowProperty("window", WND_PROP_FULLSCREEN, WINDOW_FULLSCREEN);
    imshow("window", splitChannels[channel - 1]);

    waitKey(0);

    destroyAllWindows();
}

double Imagem::rad2degree(double rad)
{
    return (rad * (180.0 / M_PI));
}

Vec4d Imagem::rgb2cmyk(const Vec3b &rgb_pixel)
{
    double c = 1.0 - (rgb_pixel.val[2] / 255.0);
    double y = 1.0 - (rgb_pixel.val[0] / 255.0);
    double m = 1.0 - (rgb_pixel.val[1] / 255.0);
    double k = min(min(c, m), y);

    Vec4d cmyk_pixel;
    cmyk_pixel[0] = (c - k) / (1 - k);
    cmyk_pixel[1] = (m - k) / (1 - k);
    cmyk_pixel[2] = (y - k) / (1 - k);
    cmyk_pixel[3] = k;

    return cmyk_pixel;
}

Vec3d Imagem::rgb2hsi(const Vec3b &rgb_pixel)
{
    double r = rgb_pixel[2] / 255.0;
    double g = rgb_pixel[1] / 255.0;
    double b = rgb_pixel[0] / 255.0;

    double theta = rad2degree(acos(((r - g) + (r - b)) /
            (2.0 * sqrt(pow(r - g, 2.0) + (r - b) * (g - b)))));

    Vec3d hsi_pixel;
    hsi_pixel[0] = b <= g ? theta : 360.0 - theta;
    hsi_pixel[1] = 1.0 - (3.0 * min(r, min(g, b))) / (r + g + b);
    hsi_pixel[2] = (r + g + b) / 3.0;

    return hsi_pixel;
}

Mat Imagem::get_cmyk_image()
{
   Mat cmyk;
   cmyk.create(image.rows, image.cols, CV_64FC4);

    for(int i = 0; i < cmyk.rows; i++){
        for(int j = 0; j < cmyk.cols; j++){
            cmyk.at<Vec4d>(i, j) = rgb2cmyk(image.at<Vec3b>(i, j));
        }
    }

    return cmyk;
}

Mat Imagem::get_hsi_image()
{
    Mat hsi;
    hsi.create(image.rows, image.cols, CV_64FC3);

    for(int i = 0; i < hsi.rows; i++){
        for(int j = 0; j < hsi.cols; j++){
            hsi.at<Vec3d>(i, j) = rgb2hsi(image.at<Vec3b>(i, j));
        }
    }

    return hsi;
}

vector<vector<double>> Imagem::get_rgb_histograms()
{
    vector<vector<double>> histograms(3, vector<double>(256, 0.0));

    for (int i = 0; i < image.rows; i++)
    {
        for (int j = 0; j < image.cols; j++)
        {
            Vec3b pixel = image.at<Vec3b>(i, j);
            histograms[0][pixel[0]] += 1;
            histograms[1][pixel[1]] += 1;
            histograms[2][pixel[2]] += 1;
        }
    }

    return histograms;
}

vector<double> Imagem::get_gray_histogram()
{
    vector<double> histogram(256, 0.0);

    for (int i = 0; i < image.rows; i++)
    {
        for (int j = 0; j < image.cols; j++)
        {
            uint8_t pixel = image.at<uint8_t>(i, j);
            histogram[pixel] += 1;
        }
    }

    return histogram;
}

Mat Imagem::discrete_cosine_transform()
{
    int channels = image.channels();
    int m = image.rows, n = image.cols;

    Mat dct;
    dct.create(m, n, CV_64FC3);

    double cu, cv, dct1, sum;

    for(int channel = 0; channel < channels; channel++){
        for (int u = 0; u < m; u++) {
            for (int v = 0; v < n; v++) {

                cu = u == 0 ? 1 / sqrt(m) : sqrt(2) / sqrt(m);
                cv = v == 0 ? 1 / sqrt(n) : sqrt(2) / sqrt(n);

                sum = 0;
                for (int x = 0; x < m; x++) {
                    for (int y = 0; y < n; y++) {
                        dct1 = image.at<Vec3b>(x, y)[channel] *
                                cos((2 * x + 1) * u * M_PI / (2 * m)) *
                                cos((2 * y + 1) * v * M_PI / (2 * n));

                        sum += dct1;
                    }
                }
                dct.at<Vec3d>(u, v)[channel] = round(cu * cv * sum);
            }
        }
    }

    return dct;
}

void Imagem::binarize(int threshold)
{
    for(int i = 0; i < image.rows; i++){
        for(int j = 0; j < image.cols; j++){
            if(image.at<uint8_t>(i, j) >= (uint8_t) threshold)
                image.at<uint8_t>(i, j) = 255;
            else
                image.at<uint8_t>(i, j) = 0;
        }
    }
}

void Imagem::normalizar()
{
    this->image.convertTo(this->image, CV_64FC3, 1.0 / 255.0);
}

int Imagem::get_cols() const
{
    return image.cols;
}

int Imagem::get_rows() const
{
    return image.rows;
}

const Mat &Imagem::getImage() const {
    return image;
}

void Imagem::setImage(const Mat &imagem) {
    Imagem::image = imagem;
}
