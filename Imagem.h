#ifndef RGB_CMYK_HSI_IMAGEM_H
#define RGB_CMYK_HSI_IMAGEM_H

#include "Dominio_frequencia.h"
#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <vector>

using namespace cv;
using namespace std;

class open_img_exception{};

class Imagem
{
    private:
        Mat image;
        static Vec4d rgb2cmyk(const Vec3b&);
        static Vec3d rgb2hsi(const Vec3b&);
        static double rad2degree(double);
    public:
        Imagem(const string&);
        Imagem(const Mat&);
        void rederizar();
        void rederizar(int);
        Mat get_cmyk_image();
        Mat get_hsi_image();
        vector<vector<double>> get_rgb_histograms();
        vector<double> get_gray_histogram();
        int get_cols() const;
        int get_rows() const;
        const Mat &getImage() const;
        void setImage(const Mat &imagem);
        Mat discrete_cosine_transform();
        void normalizar();
        void binarize(int);
};

#endif //RGB_CMYK_HSI_IMAGEM_H
