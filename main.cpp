#include "Imagem.h"
#include "Dominio_frequencia.h"
#include "Histograma.h"
#include "Plots.h"

#include <iostream>

using namespace cv;
using namespace std;

#define IMAGE_PATH "Images/mama.png"
#define MUNDO_PATH "Images/mundo.jpg"
#define MINI_LENA_PATH "Images/mini_lena.png"

void rgb_cmyk_hsi()
{
    bool terminou = false;
    int escolha_1 = 0, channel = -1;

    while(!terminou){
        cout << "----------------------------------------------------------" << endl;
        cout << "                  RGB_CMY_HSI" << endl;
        cout << "----------------------------------------------------------" << endl;
        cout << "Digite para uma das seguintes opções:" << endl;
        cout << "1 - Abrir a imagem em RGB" << endl;
        cout << "2 - Abrir a imagem em CMYK" << endl;
        cout << "3 - Abrir a imagem em HSI" << endl;
        cout << "4 - Sair" << endl;
        cout << "Opção selecionada: ";
        cin >> escolha_1;
        switch(escolha_1) {
            case 1:
                try {
                    Imagem img(IMAGE_PATH);
                    img.rederizar();
                } catch (open_img_exception){
                    cout << "Erro: nome ou caminho de arquivo errados" << endl;
                }
                break;
            case 2:
                cout << "Selecione uma das opções de visualização:" << endl;
                cout << "1 - Exibir a camada C" << endl;
                cout << "2 - Exibir a camada M" << endl;
                cout << "3 - Exibir a camada Y" << endl;
                cout << "4 - Exibir a camada K" << endl;
                cout << "Opção selecionada: ";
                cin >> channel;
                try {
                    if(channel > 0 && channel <= 4){
                        Imagem img(IMAGE_PATH);
                        Imagem cmyk(img.get_cmyk_image());
                        cmyk.rederizar(channel);
                    }
                    else
                        cout << "Erro: camada inválida" << endl;
                } catch (open_img_exception){
                    cout << "Erro: nome ou caminho de arquivo errados" << endl;
                }
                break;
            case 3:
                cout << "Selecione uma das opções de visualização:" << endl;
                cout << "1 - Exibir a camada H" << endl;
                cout << "2 - Exibir a camada S" << endl;
                cout << "3 - Exibir a camada I" << endl;
                cin >> channel;
                try {
                    if(channel > 0 && channel <= 3){
                        Imagem img(IMAGE_PATH);
                        Imagem hsi(img.get_hsi_image());
                        hsi.rederizar(channel);
                    } else {
                        cout << "Erro: camada inválida" << endl;
                    }
                } catch (open_img_exception){
                    cout << "Erro: nome ou caminho de arquivo errados" << endl;
                }
                break;
            case 4:
                terminou = true;
                break;
            default:
                cout << "Erro: opção inválida" << endl;
                break;
        }
    }
}

void img_histograma()
{
    Imagem img(IMAGE_PATH);
    Histograma hist(img.get_rgb_histograms());
    hist.normalizar_rgb(img.get_cols() * img.get_rows());
    hist.accumular();
    hist.plotar(0);
    hist.equalizar_imagem((Imagem &) img.getImage());
    Imagem imgEq(hist.equalizar_imagem(img));
    imgEq.rederizar();
}

void dom_frequencia()
{
    bool terminou = false;
    int escolha_1 = 0;

    while(!terminou){
        cout << "----------------------------------------------------------" << endl;
        cout << "                Domínio de Frequência" << endl;
        cout << "----------------------------------------------------------" << endl;
        cout << "Digite para uma das seguintes opções:" << endl;
        cout << "1 - Ver imagem com ruído" << endl;
        cout << "2 - Aplicar o filtro passa-alta na imagem com ruído" << endl;
        cout << "3 - Aplicar o filtro passa-baixa na imagem com ruído" << endl;
        cout << "4 - Sair" << endl;
        cout << "Opção selecionada: ";
        cin >> escolha_1;
        switch(escolha_1) {
            case 1:
                try {
                    Imagem lena(MINI_LENA_PATH);
                    Dominio_frequencia dom_lena(lena.discrete_cosine_transform());
                    dom_lena.insert_noise(25, 25, 5, 255.0);
                    Imagem result(dom_lena.inverse_discrete_cosine_transform());
                    result.rederizar();
                } catch (open_img_exception) {
                    cout << "Erro: nome ou caminho de arquivo errados" << endl;
                }
                break;
            case 2:
                try {
                    Imagem lena(MINI_LENA_PATH);
                    Dominio_frequencia dom_lena(lena.discrete_cosine_transform());
                    dom_lena.insert_noise(25, 25, 5, 255.0);
                    dom_lena.high_pass_filter(25, 25, 5);
                    Imagem result(dom_lena.inverse_discrete_cosine_transform());
                    result.rederizar();
                } catch (open_img_exception) {
                    cout << "Erro: nome ou caminho de arquivo errados" << endl;
                }
                break;
            case 3:
                try {
                    Imagem lena(MINI_LENA_PATH);
                    Dominio_frequencia dom_lena(lena.discrete_cosine_transform());
                    dom_lena.insert_noise(25, 25, 5, 255.0);
                    dom_lena.low_pass_filter(0, 0, 30);
                    Imagem result(dom_lena.inverse_discrete_cosine_transform());
                    result.rederizar();
                } catch (open_img_exception) {
                    cout << "Erro: nome ou caminho de arquivo errados" << endl;
                }
                break;
            case 4:
                terminou = true;
                break;
            default:
                cout << "Erro: opção inválida" << endl;
                break;
        }
    }
}

void otsu()
{
    Mat gray_lena = imread(MUNDO_PATH, IMREAD_GRAYSCALE);
    Imagem img(gray_lena);
    Histograma gray_hist(img.get_gray_histogram());
    gray_hist.normalizar_gray(gray_lena.rows * gray_lena.cols);
    img.binarize(gray_hist.otsu_method());
    img.rederizar();
}

void DDA()
{
    int x0, y0, x1, y1;

    cout << "Digite o valor de x0: ";
    cin >> x0;
    cout << "Digite o valor de y0: ";
    cin >> y0;
    cout << "Digite o valor de x1: ";
    cin >> x1;
    cout << "Digite o valor de y1: ";
    cin >> y1;

    Plots::plot2DLine(x0, y0, x1, y1);
}

int main()
{
    int escolha = -1;
    bool continuar = true;

    while(continuar){
        cout << "----------------------------------------------------------" << endl;
        cout << "                  VISÃO COMPUTACIONAL" << endl;
        cout << "----------------------------------------------------------" << endl;
        cout << "Digite para escolher um dos programas:" << endl;
        cout << "1 - Transformações RGB CMYK HSI" << endl;
        cout << "2 - Histogramas em imagens" << endl;
        cout << "3 - Domínio de frequência" << endl;
        cout << "4 - Limiarização global de Otsu" << endl;
        cout << "5 - Algoritmo de DDA de traçar retas" << endl;
        cout << "6 - Sair" << endl;
        cout << "Opção selecionada: ";
        cin >> escolha;

        switch (escolha) {
            case 1:
                rgb_cmyk_hsi();
                break;
            case 2:
                img_histograma();
                break;
            case 3:
                dom_frequencia();
                break;
            case 4:
                otsu();
                break;
            case 5:
                DDA();
                break;
            case 6:
                continuar = false;
                break;
            default:
                cout << "Erro: opção inválida" << endl;
                break;
        }
    }
    return 0;
}