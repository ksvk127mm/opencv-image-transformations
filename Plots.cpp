#include "Plots.h"
#include "Imagem.h"
#include "libs/pbPlots.hpp"
#include "libs/supportLib.hpp"
#include <vector>

using namespace std;

void Plots::plot2DLine(int x0, int y0, int x1, int y1)
{
    vector<double> X, Y;

    int dx = x1 - x0;
    int dy = y1 - y0;

    int steps = abs(dx) > abs(dy) ? abs(dx) : abs(dy);

    double Xinc = dx / (double) steps;
    double Yinc = dy / (double) steps;

    double x = x0;
    double y = y0;
    for (int i = 0; i <= steps; i++)
    {
        X.push_back(x);
        Y.push_back(y);
        x += Xinc;
        y += Yinc;
    }

    RGBABitmapImageReference *imageRef = CreateRGBABitmapImageReference();

    DrawScatterPlot(imageRef, 1920, 1080, &X, &Y);

    vector<double> *pngData = ConvertToPNG(imageRef->image);
    WriteToFile(pngData, "plot.png");
    DeleteImage(imageRef->image);

    Imagem img("plot.png");
    img.rederizar();
}